<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidKey;

class Cite extends Model
{
    use UuidKey;
    
    public $incrementing = false;
    protected $keyType = 'string';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'date',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
