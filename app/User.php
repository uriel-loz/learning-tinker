<?php

namespace App;

use App\Cite;
use App\Task;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\UuidKey;

class User extends Authenticatable
{
    use Notifiable;
    use UuidKey;

    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cites()
    {
        return $this->hasMany(Cite::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
