<?php

use App\Cite;
use App\Task;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberUser = 30;

        User::truncate();
        User::flushEventListeners();

        factory(User::class, $numberUser)->create()->each(
            function ($user)
            {
                $cite = factory(Cite::class)->make();
                $user->cites()->save($cite);

                $task = factory(Task::class)->make();
                $user->tasks()->save($task);
            }
        );
    }
}
